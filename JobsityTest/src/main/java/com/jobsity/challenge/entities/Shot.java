/**
 * 
 */
package com.jobsity.challenge.entities;

import java.io.Serializable;

/**
 * @author wavaleros
 *
 */
public class Shot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 227350986531999161L;
	private boolean fault;
	private int droppedPins;
	private boolean strike;

	public Shot() {
	}

	public Shot(boolean fault, int droppedPins, boolean strike) {
		this.fault = fault;
		this.droppedPins = droppedPins;
		this.strike = strike;
	}

	public boolean isFault() {
		return fault;
	}

	public void setFault(boolean fault) {
		this.fault = fault;
	}

	public int getDroppedPins() {
		return droppedPins;
	}

	public void setDroppedPins(int droppedPins) {
		this.droppedPins = droppedPins;
	}

	public boolean isStrike() {
		return strike;
	}

	public void setStrike(boolean strike) {
		this.strike = strike;
	}
}
