package com.jobsity.challenge.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Frame implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 197578274965300152L;
	private int frameNumber;
	private boolean spare;
	private Integer score;
	private boolean open;
	private List<Shot> shots;

	public Frame() {
		this.shots = new ArrayList<>();
	}

	public boolean getSpare() {
		return spare;
	}

	public void setSpare(boolean spare) {
		this.spare = spare;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public int getFrameNumber() {
		return frameNumber;
	}

	public void setFrameNumber(int frameNumber) {
		this.frameNumber = frameNumber;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public List<Shot> getShots() {
		return shots;
	}

	public void setShots(List<Shot> shots) {
		this.shots = shots;
	}

}
