/**
 * 
 */
package com.jobsity.challenge.entities;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author wavaleros
 *
 */
public class Player implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6293758998875320770L;

	private String name;

	private HashMap<Integer, Frame> frames;

	public Player() {
		this.frames = new HashMap<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<Integer, Frame> getFrames() {
		return frames;
	}

	public void setFrames(HashMap<Integer, Frame> frames) {
		this.frames = frames;
	}

}
