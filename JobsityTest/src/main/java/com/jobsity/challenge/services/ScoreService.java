/**
 * 
 */
package com.jobsity.challenge.services;

import com.jobsity.challenge.entities.Player;

/**
 * @author wavaleros
 *
 */
public interface ScoreService {

	public void calculateScoreForPlayer(Player p);

}
