/**
 * 
 */
package com.jobsity.challenge.services;

import com.jobsity.challenge.entities.Player;

/**
 * @author wavaleros
 *
 */
public interface PrintService {
	/**
	 * Form the string containing the format for the record
	 * 
	 * @param p
	 * @return
	 */
	public String printPlayerRecord(Player p);

}
