/**
 * 
 */
package com.jobsity.challenge.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobsity.challenge.beans.Row;
import com.jobsity.challenge.configurations.ErrorMessages;
import com.jobsity.challenge.exceptions.ValidationFileException;
import com.jobsity.challenge.services.ValidationService;

/**
 * @author wavaleros
 *
 */
@Service
public class ValidationServiceImpl implements ValidationService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jobsity.challenge.services.ValidationService#validateRow(java.lang.
	 * String)
	 */
	@Autowired
	ErrorMessages errors;
	private String WHITE_SPACE = " ";

	@Override
	public Row validateRow(String rowString) throws ValidationFileException {
		Row row = new Row();

		String[] split = rowString.split(WHITE_SPACE, 0);
		// Validate number of fields
		if (split.length != 2) {
			throw new ValidationFileException(errors.getMaxLength());
		}
		// Validate Name Allow everithing except empty strings
		if (split[0].length() < 1 || split[1].length() < 1) {
			throw new ValidationFileException(errors.getEmpty());
		}
		// Validate pin number. Allows [0-10] AND F. Cannot be empty.
		if (!split[1].matches("^(10|[0-9]|F)")) {
			throw new ValidationFileException(errors.getCharacterNotAllowed());
		}
		row.setName(split[0]);
		row.setPins(split[1]);
		return row;
	}

}
