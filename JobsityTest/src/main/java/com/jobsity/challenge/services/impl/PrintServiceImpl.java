/**
 * 
 */
package com.jobsity.challenge.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jobsity.challenge.entities.Frame;
import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.entities.Shot;
import com.jobsity.challenge.enums.FramesEnum;
import com.jobsity.challenge.enums.PinValuesEnum;
import com.jobsity.challenge.enums.PrintingNames;
import com.jobsity.challenge.services.PrintService;

/**
 * @author wavaleros
 *
 */
@Service
public class PrintServiceImpl implements PrintService {
	private String LINE_SEPARATOR = System.lineSeparator();
	private String TAB = "\t";

	@Override
	public String printPlayerRecord(Player p) {
		StringBuilder sb = new StringBuilder();
		// Print the Header
		sb.append(PrintingNames.FRAME.getValue());
		for (int i = 1; i <= FramesEnum.TEN.getValue(); i++) {
			sb.append(TAB);
			sb.append(TAB);
			sb.append(i);
		}
		// Add de name
		sb.append(LINE_SEPARATOR);
		sb.append(p.getName());
		sb.append(LINE_SEPARATOR);
		// adding the shots by frame
		sb.append(PrintingNames.PIN_FALL.getValue());
		for (int i = 1; i <= FramesEnum.TEN.getValue(); i++) {
			Frame frame = p.getFrames().get(i);
			List<Shot> shots = frame.getShots();
			// If is strike in the first 9 frames
			if ((shots.size() == 1 && shots.get(0).isStrike())) {
				sb.append(TAB);
				sb.append(TAB);
				sb.append(PinValuesEnum.STRIKE.getValue());
			} else {
				for (int j = 0; j < shots.size(); j++) {
					sb.append(TAB);
					Shot temp = shots.get(j);
					if (i == FramesEnum.TEN.getValue() && temp.isStrike()) {
						sb.append(PinValuesEnum.STRIKE.getValue());
					} else if (temp.isFault()) {
						sb.append(PinValuesEnum.FAULT.getValue());
					} else if (frame.getSpare() && j == shots.size() - 1) {
						sb.append(PinValuesEnum.SPARE.getValue());
					} else {
						sb.append(temp.getDroppedPins());
					}
				}
			}
		}
		// printing for the 10th frame

		// Adding the score
		sb.append(LINE_SEPARATOR);
		sb.append(PrintingNames.SCORE.getValue());
		for (int i = 1; i <= FramesEnum.TEN.getValue(); i++) {
			sb.append(TAB);
			sb.append(TAB);
			sb.append(p.getFrames().get(i).getScore());
		}
		sb.append(LINE_SEPARATOR);

		return sb.toString();
	}

}
