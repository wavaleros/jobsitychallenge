/**
 * 
 */
package com.jobsity.challenge.services.impl;

import org.springframework.stereotype.Service;

import com.jobsity.challenge.entities.Frame;
import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.enums.FramesEnum;
import com.jobsity.challenge.enums.MaxValuesEnum;
import com.jobsity.challenge.services.ScoreService;

/**
 * @author wavaleros
 *
 */
@Service
public class ScoreServiceImpl implements ScoreService {

	@Override
	public void calculateScoreForPlayer(Player p) {
		// Score for each frame
		int currentFrameScore = 0;
		int nextFrameScore = 0;
		int additionalFrameScore = 0;
		int currentScore = 0;
		// The idea is that we will sum until the frame 8 because the 9 sum is different
		// and 10th too.
		for (int i = 1; i <= p.getFrames().size() - 2; i++) {
			Frame temp = p.getFrames().get(i);
			currentFrameScore = 0;
			nextFrameScore = 0;
			additionalFrameScore = 0;
			currentFrameScore = temp.getShots().stream().mapToInt(x -> x.getDroppedPins()).sum();
			// if strike, next two shots
			if (currentFrameScore == MaxValuesEnum.MAX_PINS_PER_FRAME.getValue() && temp.getShots().get(0).isStrike()) {
				nextFrameScore = p.getFrames().get(i + 1).getShots().stream().mapToInt(x -> x.getDroppedPins()).sum();
				// if next is strike, go for the +2 frame first shot too
				if (nextFrameScore == MaxValuesEnum.MAX_PINS_PER_FRAME.getValue()
						&& p.getFrames().get(i + 1).getShots().size() == 1
						&& p.getFrames().get(i + 1).getShots().get(0).isStrike()) {
					additionalFrameScore = p.getFrames().get(i + 2).getShots().get(0).getDroppedPins();
				}
				// if spare, next shot
			} else if (currentFrameScore == MaxValuesEnum.MAX_PINS_PER_FRAME.getValue() && temp.getSpare()
					&& temp.getShots().size() == 2) {
				nextFrameScore = p.getFrames().get(i + 1).getShots().get(0).getDroppedPins();

			}
			currentScore += currentFrameScore + nextFrameScore + additionalFrameScore;
			temp.setScore(currentScore);
		}
		// sum for 9th frame
		int frameId = FramesEnum.NINE.getValue();
		currentFrameScore = p.getFrames().get(frameId).getShots().stream().mapToInt(x -> x.getDroppedPins()).sum();
		// if strike
		if (currentFrameScore == MaxValuesEnum.MAX_PINS_PER_FRAME.getValue()
				&& p.getFrames().get(frameId).getShots().get(0).isStrike()) {
			nextFrameScore = p.getFrames().get(frameId + 1).getShots().get(0).getDroppedPins()
					+ p.getFrames().get(frameId + 1).getShots().get(1).getDroppedPins();
		} else if (currentFrameScore == MaxValuesEnum.MAX_PINS_PER_FRAME.getValue()
				&& p.getFrames().get(frameId).getSpare() && p.getFrames().get(frameId).getShots().size() == 2) {
			// if spare
			nextFrameScore = p.getFrames().get(frameId + 1).getShots().get(0).getDroppedPins();
		}

		currentScore += currentFrameScore + nextFrameScore;
		p.getFrames().get(frameId).setScore(currentScore);
		// sum for 10th frame
		frameId = FramesEnum.TEN.getValue();
		currentFrameScore = p.getFrames().get(frameId).getShots().stream().mapToInt(x -> x.getDroppedPins()).sum();
		currentScore += currentFrameScore;
		p.getFrames().get(frameId).setScore(currentScore);
	}

}
