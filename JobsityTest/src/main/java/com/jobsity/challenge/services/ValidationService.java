/**
 * 
 */
package com.jobsity.challenge.services;

import com.jobsity.challenge.beans.Row;
import com.jobsity.challenge.exceptions.ValidationFileException;

/**
 * @author wavaleros
 *
 */
public interface ValidationService {

	/**
	 * Check the row for inconsistencies. Must have a String containing the name and
	 * the pin drop count separated by TAB. The pin drop count must be >=0 and <=10.
	 * The only literal value allowed is F, indicating a fault in the frame.
	 * 
	 * @param row String containing the row to validate
	 * @return
	 */
	public Row validateRow(String row) throws ValidationFileException;

}
