/**
 * 
 */
package com.jobsity.challenge.services.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobsity.challenge.beans.Row;
import com.jobsity.challenge.datastore.DataStore;
import com.jobsity.challenge.entities.Frame;
import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.entities.Shot;
import com.jobsity.challenge.enums.FramesEnum;
import com.jobsity.challenge.enums.MaxValuesEnum;
import com.jobsity.challenge.enums.PinValuesEnum;
import com.jobsity.challenge.exceptions.FileTemplateException;
import com.jobsity.challenge.exceptions.FrameValidationException;
import com.jobsity.challenge.exceptions.ValidationFileException;
import com.jobsity.challenge.services.ProcessingService;

/**
 * @author wavaleros
 *
 */
@Service
public class ProcessingServiceImpl implements ProcessingService {
	@Autowired
	ValidationServiceImpl validationServiceImpl;
	@Autowired
	ScoreServiceImpl scoreServiceImpl;
	@Autowired
	PrintServiceImpl printServiceImpl;
	@Autowired
	private DataStore dataStore;
	private String TAB = "\t";

	/**
	 * 
	 * 
	 * @see com.jobsity.challenge.services.ProcessingService#process(java.lang.String)
	 */
	@Override
	public String process(String filePath) throws FileTemplateException {
		String response = "";
		StringBuilder sb = new StringBuilder();
		int lineCount = 0;
		String s = "";
		try {
			s = new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String splitRow[] = s.split(TAB, 0);
		for (String line : splitRow) {
			try {
				++lineCount;
				Row row = validationServiceImpl.validateRow(line);
				Player p = this.findPlayer(row.getName());
				this.processRow(p, row);
			} catch (ValidationFileException e) {
				sb.append("Line : " + lineCount + e.getMessage() + "\n");
			} catch (FrameValidationException fve) {
				sb.append("Line : " + lineCount + fve.getMessage() + "\n");
			}
		}
		if (sb.length() == 0) {
			dataStore.getPlayers().stream().forEach(p -> scoreServiceImpl.calculateScoreForPlayer(p));

			for (Player p : dataStore.getPlayers()) {
				response += printServiceImpl.printPlayerRecord(p);
			}
		} else {
			response = "Processing the file, presented the following errors : \n" + sb.toString();
		}

		return response;
	}

	/**
	 * Identifies if the row can be processed for the Player.
	 * 
	 * @param p   Player
	 * @param row Row to be processed
	 * @throws FrameValidationException
	 */
	private void processRow(Player p, Row row) throws FrameValidationException {
		Optional<Integer> maxIdFrame = p.getFrames().keySet().stream().max(Comparator.comparingInt(Integer::valueOf));
		// If it's a frame in the list, get it to view if is already complete. if isn't
		// ,process the row
		if (maxIdFrame.isPresent()) {
			Frame f = p.getFrames().get(maxIdFrame.get());
			if (f.isOpen() && f.getFrameNumber() <= FramesEnum.TEN.getValue()) {
				// Process the row to add the pins dropped to the count
				if (f.getFrameNumber() == FramesEnum.TEN.getValue()) {
					this.processTenthFrame(f, row);
				} else {
					this.processShotForFrame(f, row);
				}
			} else if (f.getFrameNumber() <= FramesEnum.TEN.getValue()) {
				// Adding a new frame
				this.addFrameByRow(p, row);
			}
		} else {
			// Adding the first frame
			this.addFrameByRow(p, row);
		}

	}

	/**
	 * Add a new frame to the player, calculating the parameters of the shot by the
	 * Row information
	 * 
	 * @param p   Player processing the given Row
	 * @param row Row to be processed
	 */

	private void addFrameByRow(Player p, Row row) {
		Frame f = new Frame();
		// IF the ball was fault
		int pins = 0;
		try {
			pins = Integer.parseInt(row.getPins());
		} catch (Exception e) {
			pins = 0;
		}
		f.setFrameNumber(p.getFrames().size() + 1);
		Shot s = new Shot();
		if (row.getPins().compareTo(PinValuesEnum.FAULT.getValue()) == 0) {
			// If it's a fault
			s.setFault(true);
			s.setDroppedPins(0);
			f.getShots().add(s);
			f.setOpen(true);
		} else if (pins == 10) {
			// If it's a strike
			s.setDroppedPins(10);
			s.setFault(false);
			s.setStrike(true);
			f.getShots().add(s);
			f.setOpen(false);
			if (f.getFrameNumber() == FramesEnum.TEN.getValue() && f.getShots().size() < 3) {
				f.setOpen(true);
			}
			f.setSpare(false);
		} else if (pins >= 0 && pins < MaxValuesEnum.MAX_PINS_PER_FRAME.getValue()) {
			// If is the first shot of the frame
			s.setDroppedPins(pins);
			s.setFault(false);
			s.setStrike(false);
			f.getShots().add(s);
			f.setOpen(true);
			f.setSpare(false);
		}
		// Add the frame to the player record
		p.getFrames().put(f.getFrameNumber(), f);
	}

	/**
	 * Process the information in the row for the given <b>Frame</b>
	 * 
	 * @param f   Frame where the shot that belong to the row will be added;
	 * @param row Variable that represent the shot made;
	 * @throws FrameValidationException
	 */
	private void processShotForFrame(Frame f, Row row) throws FrameValidationException {
		int pins = 0;
		try {
			pins = Integer.parseInt(row.getPins());
		} catch (Exception e) {
			pins = 0;
		}
		Shot s = new Shot();
		int actualSum = f.getShots().stream().mapToInt(i -> i.getDroppedPins()).sum();
		// If it's fault, we put 0
		if (row.getPins().compareTo(PinValuesEnum.FAULT.getValue()) == 0) {
			s.setDroppedPins(pins);
			s.setFault(true);
			f.getShots().add(s);
			// verify the count not pass 10;
		} else if (actualSum + pins <= MaxValuesEnum.MAX_PINS_PER_FRAME.getValue()) {
			s.setDroppedPins(pins);
			s.setFault(false);
			s.setStrike(false);
			f.getShots().add(s);
			actualSum = f.getShots().stream().mapToInt(i -> i.getDroppedPins()).sum();
			f.setOpen(false);
			f.setSpare(false);
			if (actualSum == 10) {
				f.setSpare(true);
			}
		} else {
			throw new FrameValidationException("More pins tan permited in the frame");
		}
	}

	/**
	 * Process the 10th frame adding each shot received to the count.
	 * 
	 * @param f   Variable that represents the 10th frame
	 * @param row Variable that represent the shot made;
	 */
	private void processTenthFrame(Frame f, Row row) {
		int pins = 0;
		try {
			pins = Integer.parseInt(row.getPins());
		} catch (Exception e) {
			pins = 0;
		}
		Shot s = new Shot();
		if (f.isOpen()) {
			switch (row.getPins()) {
			case "F":
				s.setDroppedPins(pins);
				s.setFault(true);
				s.setStrike(false);
				break;
			case "0":
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
			case "6":
			case "7":
			case "8":
			case "9":
				s.setDroppedPins(pins);
				s.setFault(false);
				s.setStrike(false);
				break;
			case "10":
				s.setDroppedPins(pins);
				s.setFault(false);
				s.setStrike(true);
				break;
			}
			// add the shot to the frame
			f.getShots().add(s);
			// Close the frame
			if (f.getShots().size() == 3) {
				f.setOpen(false);
			}
		}
	}

	/**
	 * Search the player by the name, if not exist, one new player is created and
	 * added to the system
	 * 
	 * @param name Name of the player
	 * @return Player object with given name
	 */

	private Player findPlayer(String name) {
		Player p;
		Stream<Player> st = dataStore.getPlayers().stream().filter(x -> x.getName().compareTo((name)) == 0);
		Object[] stArray = st.toArray();
		if (stArray.length == 1) {
			p = (Player) stArray[0];
		} else {
			p = new Player();
			p.setName(name);
			dataStore.getPlayers().add(p);
		}
		return p;
	}
}
