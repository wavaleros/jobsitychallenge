/**
 * 
 */
package com.jobsity.challenge.services;

import com.jobsity.challenge.exceptions.FileTemplateException;

/**
 * @author wavaleros
 *
 */
public interface ProcessingService {
	public String process(String filePath) throws FileTemplateException;
}
