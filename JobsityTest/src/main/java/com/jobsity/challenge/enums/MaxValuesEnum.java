/**
 * 
 */
package com.jobsity.challenge.enums;

/**
 * @author wavaleros
 *
 */
public enum MaxValuesEnum {
	MAX_PINS_PER_FRAME(10), MIN_PINS_PER_FRAME(0);
	private int value;

	MaxValuesEnum(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
