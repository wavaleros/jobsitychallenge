/**
 * 
 */
package com.jobsity.challenge.enums;

/**
 * @author wavaleros
 *
 */
public enum PrintingNames {
	FRAME("Frame"), PIN_FALL("Pinfalls"), SCORE("Score");
	private String value;

	PrintingNames(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
