/**
 * 
 */
package com.jobsity.challenge.enums;

/**
 * @author wavaleros
 *
 */
public enum PinValuesEnum {
	FAULT("F"), STRIKE("X"), SPARE("/");
	private String value;

	PinValuesEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
