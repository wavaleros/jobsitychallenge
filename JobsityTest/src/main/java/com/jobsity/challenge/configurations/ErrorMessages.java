/**
 * 
 */
package com.jobsity.challenge.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author wavaleros
 *
 */

//@ConfigurationProperties("errors")
@Component
@PropertySource("classpath:errors.properties")
public class ErrorMessages {
	@Value("${errors.characterNotAllowed}")
	private String characterNotAllowed;
	@Value("${errors.maxLength}")
	private String maxLength;
	@Value("${errors.empty}")
	private String empty;

	public String getCharacterNotAllowed() {
		return characterNotAllowed;
	}

	public void setCharacterNotAllowed(String characterNotAllowed) {
		this.characterNotAllowed = characterNotAllowed;
	}

	public String getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}

	public String getEmpty() {
		return empty;
	}

	public void setEmpty(String empty) {
		this.empty = empty;
	}

}
