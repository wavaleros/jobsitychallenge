/**
 * 
 */
package com.jobsity.challenge.beans;

import java.io.Serializable;

/**
 * @author wavaleros
 *
 */
public class Row implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1331414635010048903L;
	private String name;
	private String pins;

	public Row() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPins() {
		return pins;
	}

	public void setPins(String pins) {
		this.pins = pins;
	}

}
