/**
 * 
 */
package com.jobsity.challenge.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import com.jobsity.challenge.exceptions.FileTemplateException;
import com.jobsity.challenge.services.impl.ProcessingServiceImpl;

/**
 * @author wavaleros
 *
 */
@ShellComponent
public class CLICommands {
	@Autowired
	ProcessingServiceImpl service;

	@ShellMethod("Process the file in the given path.")
	public String process(String filePath) {
		String response = "";
		try {
			response = service.process(filePath);
		} catch (FileTemplateException e) {
			e.printStackTrace();
		}
		return response;
	}

}
