/**
 * 
 */
package com.jobsity.challenge.datastore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jobsity.challenge.entities.Player;

/**
 * @author wavaleros
 *
 */
public class DataStore implements Serializable {
	/**
	 * 
	 */
	private static DataStore system;

	private static final long serialVersionUID = -2903095531420899762L;

	private List<Player> players;

	public static DataStore getInstace() {
		if (system == null) {
			system = new DataStore();
		}
		return system;
	}

	private DataStore() {
		this.players = new ArrayList<>();
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

}
