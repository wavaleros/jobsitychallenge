/**
 * 
 */
package com.jobsity.challenge.exceptions;

/**
 * @author wavaleros
 *
 */
public class FrameValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7413411975927900010L;

	public FrameValidationException() {

	}

	public FrameValidationException(String message) {
		super(message);
	}
}
