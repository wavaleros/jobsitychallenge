/**
 * 
 */
package com.jobsity.challenge.exceptions;

/**
 * @author wavaleros
 *
 */
public class FileTemplateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3409807402128921385L;

	/**
	 * 
	 */
	public FileTemplateException() {

	}

	/**
	 * @param message
	 */
	public FileTemplateException(String message) {
		super(message);
	}
}
