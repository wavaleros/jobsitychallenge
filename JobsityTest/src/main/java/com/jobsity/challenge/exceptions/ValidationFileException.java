/**
 * 
 */
package com.jobsity.challenge.exceptions;

/**
 * @author wavaleros
 *
 */
public class ValidationFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -847606270924993579L;

	/**
	 * 
	 */
	public ValidationFileException() {

	}

	public ValidationFileException(String message) {
		super(message);
	}

}
