package com.jobsity.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.jobsity.challenge.datastore.DataStore;

@SpringBootApplication
@ComponentScan(basePackages = { "com.jobsity.challenge" })
public class JobsityTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobsityTestApplication.class, args);
	}

	@Bean
	public DataStore dataStore() {
		return DataStore.getInstace();
	}

}
