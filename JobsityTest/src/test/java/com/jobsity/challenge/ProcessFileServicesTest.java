package com.jobsity.challenge;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobsity.challenge.configurations.ErrorMessages;
import com.jobsity.challenge.datastore.DataStore;
import com.jobsity.challenge.exceptions.FileTemplateException;
import com.jobsity.challenge.services.impl.PrintServiceImpl;
import com.jobsity.challenge.services.impl.ProcessingServiceImpl;
import com.jobsity.challenge.services.impl.ScoreServiceImpl;
import com.jobsity.challenge.services.impl.ValidationServiceImpl;

@RunWith(SpringRunner.class)
public class ProcessFileServicesTest {

	@TestConfiguration
	static class ProcessingServiceImplTestContextConfiguration {
		@Bean
		public ProcessingServiceImpl service() {
			return new ProcessingServiceImpl();
		}

		@Bean
		public ScoreServiceImpl scoreService() {
			return new ScoreServiceImpl();
		}

		@Bean
		public PrintServiceImpl printService() {
			return new PrintServiceImpl();
		}

		@Bean
		public ValidationServiceImpl validationService() {
			return new ValidationServiceImpl();
		}

		@Bean
		public ErrorMessages errors() {
			return new ErrorMessages();
		}

		@Bean
		public DataStore dataStore() {
			return DataStore.getInstace();
		}

	}

	@Autowired
	ProcessingServiceImpl service;

	// Test with the entry of Jeff. Given in the example. input by the file
	// Integration Test
	@Test
	public void test() {
		try {
			String pathInput = Paths.get("resources/JeffExample.txt").toAbsolutePath().toString();
			String pathOutput = "resources/JeffResponse.txt";
			String actual = service.process(pathInput);
			String expected = "";
			try {
				expected = new String(Files.readAllBytes(Paths.get(pathOutput)));
			} catch (IOException e) {
				e.printStackTrace();
			}
			assertTrue(expected.compareTo(actual) == 0);

		} catch (FileTemplateException e) {

		}

	}

}
