package com.jobsity.challenge;

import com.jobsity.challenge.entities.Frame;
import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.entities.Shot;
import com.jobsity.challenge.enums.MaxValuesEnum;

public class PlayerDataBuildingUtils {

	/**
	 * A normal scenario for the dropped Pins.
	 * 
	 * @param array
	 * 
	 * @return
	 */
	public static Player creatPlayerFramesForTest3(int[] array) {
		Player p = new Player();
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			Shot s = new Shot(false, 0, false);
			f.getShots().add(s);
			s = new Shot(false, array[i - 1], false);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
		}
		return p;
	}

	/**
	 * Set the player with worst frames. Doesn't have score
	 * 
	 * @return Player p with the worst frames and shot pins
	 */
	public static Player creatPlayerFramesForTest2() {
		Player p = new Player();
		Shot s = new Shot();
		s.setDroppedPins(MaxValuesEnum.MIN_PINS_PER_FRAME.getValue());
		s.setStrike(false);
		s.setFault(false);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
		}

		return p;
	}

	/**
	 * Set the player with the perfect score. Doesn't have the score
	 * 
	 * @return Player p with the perfect frame and shots
	 */
	public static Player createPlayerFramesForScoringTest() {
		Player p = new Player();

		Shot s = new Shot();
		s.setDroppedPins(MaxValuesEnum.MAX_PINS_PER_FRAME.getValue());
		s.setStrike(true);
		s.setFault(false);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
		}
		return p;
	}

	/**
	 * Set the player with the perfect score
	 * 
	 * @return Player p with the perfect frame and shots
	 */
	public static Player createPlayerPerfectFramesAndScore() {
		Player p = new Player();
		p.setName("TestSubject");
		Shot s = new Shot();
		s.setDroppedPins(MaxValuesEnum.MAX_PINS_PER_FRAME.getValue());
		s.setStrike(true);
		s.setFault(false);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			f.getShots().add(s);
			f.setScore(i * 30);
			if (i == 10) {
				f.getShots().add(s);
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
		}
		return p;
	}

	/**
	 * A normal scenario for the dropped Pins.
	 * 
	 * @param array
	 * 
	 * @return
	 */
	public static Player createPlayerForPrintingTest(int[] array) {
		Player p = new Player();
		p.setName("Tom");
		int list[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 9, 9 };
		int sumScore[] = new int[10];
		sumScore = calculateScorebyPinsDropped(list);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			Shot s = new Shot(false, 0, false);
			f.getShots().add(s);
			s = new Shot(false, array[i - 1], false);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
				f.getShots().add(s);
			}
			f.setScore(sumScore[i - 1]);
			p.getFrames().put(i, f);
		}
		return p;
	}

	/**
	 * No strikes, no spare
	 * 
	 * @param list
	 * @return
	 */
	public static int[] calculateScorebyPinsDropped(int[] list) {
		int sum[] = new int[10];
		sum[0] = list[0];
		for (int i = 1; i < sum.length - 1; i++) {
			sum[i] = sum[i - 1] + list[i];
		}
		sum[9] = sum[8] + list[9] + list[10] + list[11];
		return sum;
	}

	/**
	 * Set the player with worst frames. Doesn't have score
	 * 
	 * @return Player p with the worst frames and shot pins
	 */
	public static Player createPlayerWorstFramesAndScore() {
		Player p = new Player();
		p.setName("Paul");
		Shot s = new Shot();
		s.setDroppedPins(MaxValuesEnum.MIN_PINS_PER_FRAME.getValue());
		s.setStrike(false);
		s.setFault(false);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			f.getShots().add(s);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
			f.setScore(0);
		}
		return p;
	}

	public static Player createPlayerWorstWithFaultsFramesAndScore() {
		Player p = new Player();
		p.setName("Paul");
		Shot s = new Shot();
		s.setDroppedPins(MaxValuesEnum.MIN_PINS_PER_FRAME.getValue());
		s.setStrike(false);
		s.setFault(true);
		for (int i = 1; i <= 10; i++) {
			Frame f = new Frame();
			f.setFrameNumber(i);
			f.setOpen(false);
			f.getShots().add(s);
			f.getShots().add(s);
			if (i == 10) {
				f.getShots().add(s);
			}
			p.getFrames().put(i, f);
			f.setScore(0);
		}
		return p;
	}

}