package com.jobsity.challenge;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobsity.challenge.beans.Row;
import com.jobsity.challenge.configurations.ErrorMessages;
import com.jobsity.challenge.enums.PinValuesEnum;
import com.jobsity.challenge.exceptions.ValidationFileException;
import com.jobsity.challenge.services.impl.ValidationServiceImpl;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:errors.properties")
public class ValidationServiceImplTest {

	@TestConfiguration
	static class ValidationServiceImplContextConfiguration {

		@Bean
		public ValidationServiceImpl validationServiceImpl() {
			return new ValidationServiceImpl();
		}

		@Bean
		public ErrorMessages errors() {
			return new ErrorMessages();
		}

	}

	String WHITE_SPACE = " ";
	@Autowired
	ErrorMessages errors;

	@Autowired
	ValidationServiceImpl validationServiceImpl;

	@Test
	/**
	 * Validation of all the success cases for the entry
	 */
	public void testValidateSuccess() {
		String name = "Bill";
		String s = name + WHITE_SPACE;
		Row r;
		try {
			for (int i = 0; i <= 10; i++) {
				r = validationServiceImpl.validateRow(s.concat(i + ""));
				assertTrue(r.getName().compareTo(name) == 0);
				assertTrue(r.getPins().compareTo(i + "") == 0);
			}
			// Verification for Fault
			s = name + WHITE_SPACE + PinValuesEnum.FAULT.getValue();
			r = validationServiceImpl.validateRow(s);
			assertTrue(r.getName().compareTo(name) == 0);
			assertTrue(r.getPins().compareTo(PinValuesEnum.FAULT.getValue()) == 0);
		} catch (ValidationFileException e) {
			e.printStackTrace();
		}
	}

	@Test
	/**
	 * Validation of different field count
	 */
	public void testValidateRowLength() {
		String a = "row";
		assertThatExceptionOfType(ValidationFileException.class).isThrownBy(() -> validationServiceImpl.validateRow(a))
				.withMessage(errors.getMaxLength());
		String b = "row" + WHITE_SPACE;
		assertThatExceptionOfType(ValidationFileException.class).isThrownBy(() -> validationServiceImpl.validateRow(b))
				.withMessage(errors.getMaxLength());
		String c = "col" + WHITE_SPACE + "col" + WHITE_SPACE + "col";
		assertThatExceptionOfType(ValidationFileException.class).isThrownBy(() -> validationServiceImpl.validateRow(c))
				.withMessage(errors.getMaxLength());
	}

	@Test
	/**
	 * Empty fields
	 */
	public void testValidateFile() {
		String a1 = "" + WHITE_SPACE + "1";
		assertThatExceptionOfType(ValidationFileException.class).isThrownBy(() -> validationServiceImpl.validateRow(a1))
				.withMessage(errors.getEmpty());
	}

}
