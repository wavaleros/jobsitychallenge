/**
 * 
 */
package com.jobsity.challenge;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.enums.FramesEnum;
import com.jobsity.challenge.services.impl.ScoreServiceImpl;

/**
 * @author wavaleros
 *
 */
@RunWith(SpringRunner.class)
public class ScoreServiceTest {

	@TestConfiguration
	static class ScoreServiceTestContextConfiguration {

		@Bean
		public ScoreServiceImpl scoreService() {
			return new ScoreServiceImpl();
		}
	}

	@Autowired
	ScoreServiceImpl scoreServiceImpl;

	/**
	 * Test for perfect game;Score 300
	 */
	@Test
	public void test() {
		Player p = PlayerDataBuildingUtils.createPlayerFramesForScoringTest();
		scoreServiceImpl.calculateScoreForPlayer(p);
		assertEquals(30, p.getFrames().get(FramesEnum.ONE.getValue()).getScore().intValue());
		assertEquals(60, p.getFrames().get(FramesEnum.TWO.getValue()).getScore().intValue());
		assertEquals(90, p.getFrames().get(FramesEnum.THREE.getValue()).getScore().intValue());
		assertEquals(120, p.getFrames().get(FramesEnum.FOUR.getValue()).getScore().intValue());
		assertEquals(150, p.getFrames().get(FramesEnum.FIVE.getValue()).getScore().intValue());
		assertEquals(180, p.getFrames().get(FramesEnum.SIX.getValue()).getScore().intValue());
		assertEquals(210, p.getFrames().get(FramesEnum.SEVEN.getValue()).getScore().intValue());
		assertEquals(240, p.getFrames().get(FramesEnum.EIGHT.getValue()).getScore().intValue());
		assertEquals(270, p.getFrames().get(FramesEnum.NINE.getValue()).getScore().intValue());
		assertEquals(300, p.getFrames().get(FramesEnum.TEN.getValue()).getScore().intValue());
	}

	/**
	 * Test for worst game; Score 0
	 */
	@Test
	public void test2() {
		Player p = PlayerDataBuildingUtils.creatPlayerFramesForTest2();
		scoreServiceImpl.calculateScoreForPlayer(p);
		assertEquals(0, p.getFrames().get(FramesEnum.ONE.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.TWO.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.THREE.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.FOUR.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.FIVE.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.SIX.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.SEVEN.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.EIGHT.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.NINE.getValue()).getScore().intValue());
		assertEquals(0, p.getFrames().get(FramesEnum.TEN.getValue()).getScore().intValue());
	}

	/**
	 * Progressive score accumulation for the normal test
	 */
	@Test
	public void test3() {
		int list[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 9, 9 };
		int sumScore[] = new int[10];
		sumScore = PlayerDataBuildingUtils.calculateScorebyPinsDropped(list);
		Player p = PlayerDataBuildingUtils.creatPlayerFramesForTest3(list);
		scoreServiceImpl.calculateScoreForPlayer(p);
		assertEquals(sumScore[0], p.getFrames().get(FramesEnum.ONE.getValue()).getScore().intValue());
		assertEquals(sumScore[1], p.getFrames().get(FramesEnum.TWO.getValue()).getScore().intValue());
		assertEquals(sumScore[2], p.getFrames().get(FramesEnum.THREE.getValue()).getScore().intValue());
		assertEquals(sumScore[3], p.getFrames().get(FramesEnum.FOUR.getValue()).getScore().intValue());
		assertEquals(sumScore[4], p.getFrames().get(FramesEnum.FIVE.getValue()).getScore().intValue());
		assertEquals(sumScore[5], p.getFrames().get(FramesEnum.SIX.getValue()).getScore().intValue());
		assertEquals(sumScore[6], p.getFrames().get(FramesEnum.SEVEN.getValue()).getScore().intValue());
		assertEquals(sumScore[7], p.getFrames().get(FramesEnum.EIGHT.getValue()).getScore().intValue());
		assertEquals(sumScore[8], p.getFrames().get(FramesEnum.NINE.getValue()).getScore().intValue());
		assertEquals(sumScore[9], p.getFrames().get(FramesEnum.TEN.getValue()).getScore().intValue());
	}
}
