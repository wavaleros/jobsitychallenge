/**
 * 
 */
package com.jobsity.challenge;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobsity.challenge.entities.Player;
import com.jobsity.challenge.enums.PrintingNames;
import com.jobsity.challenge.services.impl.PrintServiceImpl;

/**
 * @author wavaleros
 *
 */
@RunWith(SpringRunner.class)
public class PrintServiceTest {
	@TestConfiguration
	static class PrintServiceTestContextConfiguration {

		@Bean
		public PrintServiceImpl printService() {
			return new PrintServiceImpl();
		}
	}

	@Autowired
	PrintServiceImpl printService;

	/**
	 * Test for perfect game;Score 300
	 */
	@Test
	public void perfectGameTest() {
		Player p = PlayerDataBuildingUtils.createPlayerPerfectFramesAndScore();
		String s = printService.printPlayerRecord(p);
		assertTrue(s.contains(p.getName()));
		assertTrue(s.chars().filter(c -> c == '\n').count() == 4);
		assertTrue(s.contains(PrintingNames.FRAME.getValue()));
		assertTrue(s.contains(PrintingNames.PIN_FALL.getValue()));
		assertTrue(s.contains(PrintingNames.SCORE.getValue()));

	}

	/**
	 * Test for normal run. No Strikes, no Faults
	 */
	@Test
	public void normalGameTest() {
		Player p = PlayerDataBuildingUtils.createPlayerPerfectFramesAndScore();
		String s = printService.printPlayerRecord(p);
		assertTrue(s.contains(p.getName()));
		assertTrue(s.chars().filter(c -> c == '\n').count() == 4);
		assertTrue(s.contains(PrintingNames.FRAME.getValue()));
		assertTrue(s.contains(PrintingNames.PIN_FALL.getValue()));
		assertTrue(s.contains(PrintingNames.SCORE.getValue()));
	}

	/**
	 * Test for worst run. No Strikes, no Faults
	 */
	@Test
	public void worstGameTest() {
		Player p = PlayerDataBuildingUtils.createPlayerWorstFramesAndScore();
		String s = printService.printPlayerRecord(p);
		assertTrue(s.contains(p.getName()));
		assertTrue(s.chars().filter(c -> c == '\n').count() == 4);
		assertTrue(s.contains(PrintingNames.FRAME.getValue()));
		assertTrue(s.contains(PrintingNames.PIN_FALL.getValue()));
		assertTrue(s.contains(PrintingNames.SCORE.getValue()));
	}

	/**
	 * Test for worst run. No Strikes
	 */
	@Test
	public void worstWithFaultsGameTest() {
		Player p = PlayerDataBuildingUtils.createPlayerWorstWithFaultsFramesAndScore();
		String s = printService.printPlayerRecord(p);
		assertTrue(s.contains(p.getName()));
		assertTrue(s.chars().filter(c -> c == '\n').count() == 4);
		assertTrue(s.contains(PrintingNames.FRAME.getValue()));
		assertTrue(s.contains(PrintingNames.PIN_FALL.getValue()));
		assertTrue(s.contains(PrintingNames.SCORE.getValue()));
	}

}
