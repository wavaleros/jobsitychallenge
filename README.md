La aplicación ha sido desarrollado con los proyectos:
- SpringBoot 2.1.0
- Spring Shell 2.0
- JUnit 4
- Maven

La función de que ejerce el proyecto de Spring Shell, es permitir crear comandos de consola de manera práctica y eficiente para agilizar el trabajo. Al ir acompañado de SpringBoot, permite utilizar muchas de las ventajas del frameWork de Spring que está reflejadas en el código. 

Instrucciones de compilación:

Utilizar SpringToolSuite o Eclipse, para importar el proyecto como un proyecto maven y actualizar las dependencias. Y luego proceder a hacer sobre el proyecto click derecho -> Run as
-> maven build . Definir los goals de maven agregando: "clean install package". 

Ejecución: 
Hacer clic derecho sobre el proyecto e indicar Run As -> Java Application

Cuando se abra la consola, verifcar que se ejecute y cuando termine de cargar, aparecerá 
que se ha ingresado a una consola nueva en la cual se pueden ejecutar comandos.
Tendrá la palabra shell:>
Para ver los comandos disponibles escribir:
help
y pulsar enter.

Para procesar el archivo que se desee probar escribir: 
process <ruta/completa/del/archivo.txt>
Ej: 
process /home/admin/Test1/JobsityTest/resources/JeffExample.txt
y dar enter. 

Allí se imprimiran los errores si hay dentro del archivo, o la salida del procesamiento del mismo, con la estructura definida.





